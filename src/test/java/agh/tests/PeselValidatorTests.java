package agh.tests;

import agh.qa.PeselValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselValidatorTests {
    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"44051401359", true},
                {"a123456", false},
                {"440512013590", false},
                {"20222318334", true},
                {"19222098492", true},
                {"20243051759", true},
                {"44851401359", true},
                {"44251401359", true},
                {"44443101359", true},
                {"44653401359", true}
        };
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPesel(String pesel, boolean shouldBeValid){
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
    }
}
