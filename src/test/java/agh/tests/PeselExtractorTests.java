package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselExtractor;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselExtractorTests {

    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"44051401359"}
        };
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPeselDate(String pesel){
        byte[] pesel_no = new byte[11];
        pesel_no[0] = 1;
        pesel_no[1] = 1;
        pesel_no[2] = 1;
        pesel_no[3] = 1;
        pesel_no[4] = 1;
        pesel_no[9] = 5;
        Pesel pesel_number = new Pesel(pesel_no);
        PeselExtractor extractor = new PeselExtractor(pesel_number);
        String assert_birth_date = "1911-11-10";

        Assert.assertEquals(assert_birth_date, extractor.GetBirthDate());
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPeselFemale(String pesel){
        byte[] pesel_no = new byte[11];
        pesel_no[0] = 1;
        pesel_no[1] = 1;
        pesel_no[2] = 1;
        pesel_no[3] = 1;
        pesel_no[4] = 1;
        pesel_no[9] = 5;
        Pesel pesel_number = new Pesel(pesel_no);
        PeselExtractor extractor = new PeselExtractor(pesel_number);
        String sex = "Female";
        int i = -1;

        Assert.assertEquals(sex, extractor.GetSex());
        Assert.assertEquals(i, pesel_number.getDigit(-1));
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPeselMale(String pesel){
        byte[] pesel_no = new byte[11];
        pesel_no[0] = 1;
        pesel_no[1] = 1;
        pesel_no[2] = 1;
        pesel_no[3] = 1;
        pesel_no[4] = 1;
        pesel_no[9] = 4;
        Pesel pesel_number = new Pesel(pesel_no);
        PeselExtractor extractor = new PeselExtractor(pesel_number);
        String sex = "Male";

        Assert.assertEquals(sex, extractor.GetSex());
    }
}
