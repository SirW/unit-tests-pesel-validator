package agh.qa;

import java.time.LocalDate;

public class PeselExtractor {
    private Pesel pesel;

    public PeselExtractor(Pesel pesel) {
        this.pesel = pesel;
    }


    public LocalDate GetBirthDate() {
        return LocalDate.of(pesel.getBirthYear(), pesel.getBirthMonth(), pesel.getBirthDay());
    }

    public String GetSex() {
        byte digit = pesel.getDigit(9);

        if(digit % 2 == 0)
            return "Male";

        return "Female";
    }
}
